﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentBonus
{
    public partial class Divisible : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void PrimeCheck_Click(object sender, EventArgs e)
        {
            int num = int.Parse(Prime.Text);

            //value zero and one are not prime numbers
            if( num == 0 || num ==1)
            {
                result.InnerHtml = num + " is not a prime number.";
            }
            
            else
            {
                for (int i = 2; i <= num / 2; i++)
                {
                    if(num % i == 0)
                    {
                        result.InnerHtml = num + " is not a prime numnber.";
                        return;
                    }
                }
                result.InnerHtml = num + " is a prime number.";
            }
                
                
        }
    }
}