﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Divisible.aspx.cs" Inherits="AssignmentBonus.Divisible" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server">Choose a number:</asp:Label>
            <br />
            <asp:TextBox runat="server" ID="Prime"></asp:TextBox>
            <asp:RequiredFieldValidator ID="NotEmpty" runat="server" ErrorMessage="Field Cant be left Blank." ControlToValidate="Prime"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="NumberValidator" ControlToValidate="Prime" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <!--For this RegularExpressionValidator, I got the way to allow only numbers to be entered in the Prime textBox by using ValidationExpression "\d+" 
                https://stackoverflow.com/questions/9732455/how-to-allow-only-integers-in-a-textbox/9732876#9732876 This is the link where i got it from.-->
            <br />
            <asp:Button runat="server" Text="IS IT PRIME??" ID="PrimeCheck" OnClick="PrimeCheck_Click" />
            <br />
            
        </div>
        <div id="result" runat="server" style="font-weight:bold; color:#C00000;">
        </div>
    </form>
</body>
</html>
