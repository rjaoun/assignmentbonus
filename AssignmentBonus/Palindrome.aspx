﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="AssignmentBonus.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server">Enter a String:</asp:Label>
            <br />
            <asp:TextBox runat="server" ID="PalindromeString"></asp:TextBox>
            <!-- ValidationExpression I added \s to allow spaces -->
            <asp:RegularExpressionValidator ID="StringValidator" ControlToValidate="PalindromeString" runat="server" ErrorMessage="Only Strings allowed" ValidationExpression="^[A-Za-z\s]+$"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="NotEmpty" runat="server" ErrorMessage="Field Cant be left Blank." ControlToValidate="PalindromeString"></asp:RequiredFieldValidator>
            <br />
            <asp:Button runat="server" Text="IS IT PALINDROME??" ID="PalindromeBtn" OnClick="PalindromeBtn_Click" />
            <br />
        </div>
        <div id="result" runat="server" style="font-weight:bold; color:#C00000;">
        </div>
    </form>
</body>
</html>
