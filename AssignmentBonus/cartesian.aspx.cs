﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentBonus
{
    public partial class cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ResultButton_Click(object sender, EventArgs e)
        {
            /*We assign two values, first will hold the Xaxis value which is axisX,
             * second will hold Yaxis value which is axisY.
             * Then, we compare both values to see where they fall.
             */ 
            int axisX = int.Parse(Xaxis.Text);
            int axisY = int.Parse(Yaxis.Text);

            if(axisX > 0 && axisY > 0)
            {
                result.InnerHtml = "("+axisX + "," +axisY + ") <br />";
                result.InnerHtml += "These points fall in Quadrant 1.";
            }
            else if(axisX < 0 && axisY > 0)
            {
                result.InnerHtml = "(" + axisX + "," + axisY + ") <br />";
                result.InnerHtml += "These points fall in Quadrant 2.";
            }
            else if (axisX < 0 && axisY < 0)
            {
                result.InnerHtml = "(" + axisX + "," + axisY + ") <br />";
                result.InnerHtml += "These points fall in Quadrant 3.";
            }
            else
            {
                result.InnerHtml = "(" + axisX + "," + axisY + ") <br />";
                result.InnerHtml += "These points fall in Quadrant 4.";
                
            }



        }
    }
}