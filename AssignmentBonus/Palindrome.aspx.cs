﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentBonus
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void PalindromeBtn_Click(object sender, EventArgs e)
        {
            /*First we assign value CheckString to become lowercase and replacing spaces with empty strings
             *then we declare another value named ReversedString with value "" which will hold the string in 
             * reverse. After that we use for loop initializing i to the length of the string minus 1 since
             * the string starts at 0, with a condtion that i is greater or equal to zero, and i incremented
             * after each iteration.
             * we add the strings backwords to the ReversedString and after the loop is done we compare 
             * the ReversedString with CheckedString, if they match it means it palindrome word else it
             * is not.
             */ 



            string CheckedString = PalindromeString.Text.ToLower().Replace(" ",string.Empty) ;
            string ReversedString="";

            for (int i = CheckedString.Length -1; i >=0; i--)
            {
                ReversedString = ReversedString + CheckedString[i];
            }
            if ( ReversedString == CheckedString)
            {
                result.InnerHtml = "The word is Palindrome";
            }
            else
            {
                result.InnerHtml = "The word is not Palindrome";
            }

        }
    }
}