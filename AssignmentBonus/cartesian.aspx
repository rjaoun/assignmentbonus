﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cartesian.aspx.cs" Inherits="AssignmentBonus.cartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server">Choose integer for X-axis:</asp:Label>
            <br />
            <asp:TextBox runat="server" ID="Xaxis"></asp:TextBox>
            <asp:CompareValidator runat="server" ID="NotEqualZero" ValueToCompare="0" ControlToValidate="Xaxis" ErrorMessage="Must not be zero value or string" Operator="NotEqual" Type="Integer"></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="NotEmpty" runat="server" ErrorMessage="Field Cant be left Blank." ControlToValidate="Xaxis"></asp:RequiredFieldValidator>
            <br />
            <asp:Label runat="server">Choose integer for Y-axis:</asp:Label>
            <br />
            <asp:TextBox runat="server" ID="Yaxis"></asp:TextBox>
            <asp:CompareValidator runat="server" ID="NotEqualToZero" ValueToCompare="0" ControlToValidate="Yaxis" ErrorMessage="Must not be zero value or string" Operator="NotEqual" Type="Integer"></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="IsNotEmpty" runat="server" ErrorMessage="Field Cant be left Blank." ControlToValidate="Yaxis"></asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="ResultButton" runat="server" OnClick="ResultButton_Click" Text="Button" /><br />

        </div>

        <div id="result" runat="server" style="font-weight:bold; color:#C00000;">
        </div>
    </form>
</body>
</html>
